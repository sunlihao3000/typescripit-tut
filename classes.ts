interface UserInterface{
    name: string;
    email: string;
    age: number;
    register();
    payInvoice();
}

class User implements UserInterface {
    name: string;
    email: string;
    age: number;

    constructor(name:string, eamil:string, age:number) {
        // this --- mean this Class which you create
        this.name = name;
        this.email = eamil;
        this.age = age;

        console.log('User Craeted: '+ this.name);
    }

    // it could use private and public too 
    register(){
        console.log(this.name + 'is now register');
    }
    payInvoice(){
        console.log(this.name+' is payed Invoice');
    }
}

class Member extends User {
    id:number;
    
    constructor(id: number, name:string, email:string, age:number){
        super(name,email,age);
        // super does         
        // this.name = name;
        // this.email = eamil;
        // this.age = age;
        
        this.id = id;
    }
    payInvoice(){
        super.payInvoice();
    };
}

// let john = new User('John Doe', 'jdoe@gmail.com',41);
// console.log(john.age);
// john.register();

let mark = new Member(1,'Mark', 'mark@gmail.com',35);
mark.payInvoice();

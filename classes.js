var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var User = (function () {
    function User(name, eamil, age) {
        // this --- mean this Class which you create
        this.name = name;
        this.email = eamil;
        this.age = age;
        console.log('User Craeted: ' + this.name);
    }
    // it could use private and public too 
    User.prototype.register = function () {
        console.log(this.name + 'is now register');
    };
    User.prototype.payInvoice = function () {
        console.log(this.name + ' is payed Invoice');
    };
    return User;
}());
var Member = (function (_super) {
    __extends(Member, _super);
    function Member(id, name, email, age) {
        var _this = _super.call(this, name, email, age) || this;
        // super does         
        // this.name = name;
        // this.email = eamil;
        // this.age = age;
        _this.id = id;
        return _this;
    }
    Member.prototype.payInvoice = function () {
        _super.prototype.payInvoice.call(this);
    };
    ;
    return Member;
}(User));
// let john = new User('John Doe', 'jdoe@gmail.com',41);
// console.log(john.age);
// john.register();
var mark = new Member(1, 'Mark', 'mark@gmail.com', 35);
mark.payInvoice();
